﻿using System;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.Logging;
using Microsoft.eShopWeb.ApplicationCore.Entities.OrderAggregate;

namespace Microsoft.eShopWeb.ApplicationCore.Services
{
    public class ServiceBusService
    {
        private readonly string _serviceBusConnectionString;
        private readonly string _queueName;
        private readonly ILogger<ServiceBusService> _logger;
        private readonly ServiceBusSender _serviceBusSender;

        public ServiceBusService(ILogger<ServiceBusService> logger)
        {
            _serviceBusConnectionString = Environment.GetEnvironmentVariable("ServiceBusConnection");
            _queueName = Environment.GetEnvironmentVariable("OrdersQueueName");
            _logger = logger;
            var serviceBusClient = new ServiceBusClient(_serviceBusConnectionString);
            _serviceBusSender = serviceBusClient.CreateSender(_queueName);
        }

        public async Task<bool> SendAsync(Order order)
        {
            try
            {
                var jsonOrder = order?.ToJson();
                _logger.LogTrace("order: " + jsonOrder);
                var message = new ServiceBusMessage(jsonOrder);
                message.ContentType = "Application/Json";
                await _serviceBusSender.SendMessageAsync(message);
                _logger.LogInformation(order?.Id + " is sent to the service bus");
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, "Fail to send order to the service bus...");
                return false;
            }

             return true;
        }
    }
}