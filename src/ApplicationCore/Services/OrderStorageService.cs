﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.eShopWeb.ApplicationCore.Entities.OrderAggregate;

namespace Microsoft.eShopWeb.ApplicationCore.Services
{
    public class OrderStorageService
    {
        private readonly HttpClient _httpClient;
        private readonly string _apiUrl;
        private readonly ILogger<OrderStorageService> _logger;

        public OrderStorageService(
            HttpClient httpClient,
            ILogger<OrderStorageService> logger)
        {
            _httpClient = httpClient;
            _apiUrl = Environment.GetEnvironmentVariable("STORAGE_API_URL");
            _logger = logger;
        }

        public async Task<bool> SendToStorage(Order order)
        {
            try
            {
                _logger.LogTrace("order: " + order?.ToJson());
                var createOrderUrl = CreateOrderUrl(_apiUrl, order);
                var stringContent = new StringContent(order.ToJson());
                await _httpClient.PostAsync(createOrderUrl, stringContent);
                _logger.LogInformation(order?.Id + " is sent to storage");
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, "Fail to send order to the storage...");
                return false;
            }

             return true;
        }

        private static string CreateOrderUrl(string baseUrl, Order order)
            => $"{baseUrl}&id={order.Id}&buyerId={order.BuyerId}"; 
    }
}